var should = require('should');
var Q = require('q');
var fs = require('fs');
var debug = require('debug')('noderank:test');

describe('noderank', function pagerankTest() {
  this.timeout(0);
  /*
  before(function beforeRunningTests(done) {
    var pr = require('../noderank');
    promise = pr();
    done();
  });
  it('should return a promise', function doIt(done) {
    var debug = require('debug')('noderank:test:pagerankTest:doIt');
    debug('duck-typing the response');
    promise.then.should.be.type('function');
    done();
  });
  */
  it('should include a reference to npmstats object', function doIt(done) {
    var nr = require('../noderank');
    nr.nodeRank.should.be.ok;
    nr.npmStats.should.be.ok;
    done();
  });
  it('should reject the returned promise if an error happens building dependency graph', function doIt(done) {
    var nr = require('../noderank').nodeRank;
    var npmstatsMock = {};
    npmstatsMock.buildDependencyGraph = function() {
      debug('mock buildDependencyGraph');
      var mockDependencyGraphPromise = Q.defer();
      setTimeout(function onTimeout() {
        var debug = require('debug')('noderank:test:pagerankTest:doIt:npmstatsMock.buildDependencyGraph:onTimeout');
        debug('rejecting mock promise');
        mockDependencyGraphPromise.reject(new Error('buildDependencyGraph error'));
      }, 100);
      return mockDependencyGraphPromise.promise;
    };
    var promise = nr(npmstatsMock);
    promise.then(function onPromiseResolved(response) {
      var debug = require('debug')('noderank:test:pagerankTest:doIt:onPromiseResolved');
      debug('noderank response\n%j', response);
      should.fail('promise was resolved');
      done(new Error('promise was resolved'));
    }, function onPromiseRejected(error) {
      debug('promise rejected %j', error.message);
      error.message.should.be.ok;
      should(error).match(/buildDependencyGraph error/);
      done();
    });
  });
  it('should resolve the returned promise if no error happens when processing', function doIt(done) {
    var nr = require('../noderank').nodeRank;
    var npmstatsMock = {};
    npmstatsMock.buildDependencyGraph = function() {
      var mockDependencyGraphPromise = Q.defer();
      setTimeout(function onTimeout() {
        mockDependencyGraphPromise.resolve({});
      }, 100);
      return mockDependencyGraphPromise.promise;
    };
    npmstatsMock.normalizeDependencyGraph = function() {
      var mockNormalizedDependencyGraph = {
        '#0': ['#1', '#2'],
        '#1': ['#0', '#2'],
        '#2': ['#0', '#1']
      };
      return mockNormalizedDependencyGraph;
    };
    var promise = nr(npmstatsMock);
    promise.then(function onPromiseResolved(response) {
      debug('noderank response\n%j', response);
      fs.writeFileSync('pagerank_response.json', JSON.stringify(response));
      response['0'].should.equal(0.3333333333333333);
      response['1'].should.equal(0.3333333333333333);
      response['2'].should.equal(0.3333333333333333);
      done();
    }, function onPromiseRejected(err) {
      debug('promise rejected');
      should(err).not.be.ok(err);
      should.fail('promise was rejected');
      done(err);
    });
  });
});
