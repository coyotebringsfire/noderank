var should = require('should');
var _debug = require('debug');
var debug = _debug('noderank:npmstats:test');
var NpmStats = require('../lib/npmstats');
var async = require('async');
var Q = require('q');

describe('npmstats', function() {
  this.timeout(0);
  /*
  it('should return a NpmStats object with default url if not passed options', function doIt(done) {
    var npmstats = new NpmStats();
    npmstats.url.should.be.ok;
    done();
  });
  it('should return a NpmStats object with default url if not passed a url in options', function doIt(done) {
    var npmstats = new NpmStats({});
    npmstats.url.should.be.ok;
    done();
  });
  it('should return a NpmStats object with custom url if url is passed in options', function doIt(done) {
    var npmstats = new NpmStats({
      url:'TESTURL'
    });
    npmstats.url.should.equal('TESTURL');
    done();
  });
  */
  describe('getAllModules', function getAllModules() {
    var modulesPromise;
    var npmstats;
    before(function startDownloadingStats() {
      debug('calling npmstats');
      npmstats = new NpmStats();
      modulesPromise = npmstats.getAllModules();
    });
    it('should return a promise', function doIt(done) {
      debug('duck-typing promise');
      modulesPromise.then.should.be.type('function');
      done();
    });
    it('should resolve returned promise with list of all npm modules', function doIt(done) {
      modulesPromise.then(function onPromiseResolved(modules) {
        debug('returned module length: %d', modules.length);
        debug('modules %j', modules);
        should(modules).be.ok;
        done();
      }, function onPromiseRejected(err) {
        should.fail('promise was rejected');
        done(err);
      });
    });
  });
  describe('getDependencies', function getDependencies() {
    var npmstats;
    var allModules;
    before(function startDownloadingStats(done) {
      debug('calling npmstats');
      npmstats = new NpmStats();
      npmstats.getAllModules()
        .then(function onGetAllModulesResolved(modules) {
          debug('modules resolved %j', modules);
          allModules = modules;
          done();
        }, function onGetAllModulesRejected(err) {
          debug('error getting modules: %j', err);
          done(err);
        });
    });
    it('should return a promise', function doIt(done) {
      var p = npmstats.getDependencies(allModules[0]);
      p.then.should.be.type('function');
      p.then(function onGetDependenciesResolved() {
        done();
      }, function onGetDependenciesRejected(err) {
        should.fail('getDependencies promise was rejected: %j', err);
      });
    });
    it('should resolve the returned promise with an array of dependencies', function doIt(done) {
      var dgraph = {};
      async.each(allModules.slice(0, 0), function forEachModule(module, nextModule) {
        debug('getting dependencies for %s', module);
        npmstats.getDependencies(module)
          .then(function onGetDependenciesResolved(dependencies) {
            debug('dependencies %j', dependencies);
            dependencies.length.should.be.type('number');
            dgraph[module] = dependencies;
            nextModule();
          }, function onGetDependenciesRejected(err) {
            should.fail('getDependencies rejected returned promise: %j', err);
            nextModule(err);
          });
      }, function doneGettingAllDependencies(err) {
        debug('all dependencies gotten %j', dgraph);
        done(err);
      });
    });
    it('should fetch npm info for each module', function doIt(done) {
      var dgraph = {};
      async.each(allModules.slice(0, 10), function forEachModule(module, nextModule) {
        debug('getting dependencies for %s', module);
        npmstats.getDependencies(module)
          .then(function onGetDependenciesResolved(dependencies) {
            debug('dependencies %j', dependencies);
            dependencies.length.should.be.type('number');
            dgraph[module] = dependencies;
            debug('module: %j', npmstats[module]);
            nextModule();
          }, function onGetDependenciesRejected(err) {
            should.fail('getDependencies rejected returned promise: %j', err);
            nextModule(err);
          });
      }, function doneGettingAllDependencies(err) {
        debug('npmstats: %j', npmstats.allModules);
        debug('all dependencies gotten %j', dgraph);
        done(err);
      });
    });
  });
  describe('buildDependencyGraph', function buildDependencyGraph() {
    var graphPromise;
    var npmstats;
    before(function startBuildingGraph(done) {
      debug('calling npmstats');
      npmstats = new NpmStats();
      npmstats.getAllModules = function() {
        var mockPromise = Q.defer();
        setTimeout(function() {
          var mockModules = {
            a: ['b', 'c'], b: ['a', 'c'], c: ['a', 'b']
          };
          npmstats.getDependencies = function(module) {
            var mockDependencyPromise = Q.defer();
            setTimeout(function() {
              debug('resolving getDependencies(%s) with %j', module, mockModules[module]);
              mockDependencyPromise.resolve(mockModules[module]);
            }, 100);
            return mockDependencyPromise.promise;
          };
          debug('resolving getAllModules with %j', Object.keys(mockModules));
          mockPromise.resolve(Object.keys(mockModules));
        }, 100);
        return mockPromise.promise;
      };
      graphPromise = npmstats.buildDependencyGraph();
      done();
    });
    it('should return a promise', function doIt(done) {
      graphPromise.then.should.be.type('function');
      done();
    });
    // this takes a really long time on production
    it('should resolve the returned promise with a dependency graph', function doIt(done) {
      graphPromise
        .then(function onBuildDependencyGraphResolved(graph) {
          graph.should.be.ok;
          done();
        }, function onBuildDependencyGraphRejected(err) {
          should.fail('graph promise rejected: %j', err);
          done(err);
        });
    });
  });
  describe('normalizeDependencyGraph', function indexDependencyGraph() {
    var npmstats;
    var graphPromise;
    it('should prepend all modules names and dependencies with default token', function doIt(done) {
      debug('calling npmstats');
      npmstats = new NpmStats();
      npmstats.getAllModules = function() {
        var mockPromise = Q.defer();
        setTimeout(function() {
          var mockModules = {
            a: ['b', 'c'], b: ['a', 'c'], c: ['a', 'b']
          };
          npmstats.getDependencies = function(module) {
            var mockDependencyPromise = Q.defer();
            setTimeout(function() {
              mockDependencyPromise.resolve(mockModules[module]);
            }, 100);
            return mockDependencyPromise.promise;
          };
          mockPromise.resolve(Object.keys(mockModules));
        }, 100);
        return mockPromise.promise;
      };
      graphPromise = npmstats.buildDependencyGraph();
      graphPromise
        .then(function onNormalizeDependencyGraphResolved(graph) {
          var expectedResult = {
            '#a': ['#b', '#c'], '#b': ['#a', '#c'], '#c': ['#a', '#b']
          };
          debug('buildDependencyGraph resolved %j', graph);
          graph.should.be.ok;
          var normalizedGraph = npmstats.normalizeDependencyGraph(graph);
          debug('normalizedGraph %j', normalizedGraph);
          JSON.stringify(normalizedGraph).should
            .equal(JSON.stringify(expectedResult));
          done();
        }, function onNormalizeDependencyGraphRejected(err) {
          should.fail('graph promise rejected: %j', err);
          done(err);
        });
    });
  });
  describe('get', function() {
    var detailsPromise;
    var npmstats;
    before(function startDownloadingStats() {
      debug('calling npmstats');
      npmstats = new NpmStats();
      detailsPromise = npmstats.get('noderank');
    });
    it('should return a promise', function doIt(done) {
      debug('duck-typing promise');
      detailsPromise.then.should.be.type('function');
      done();
    });
    it('should return details for the specified module', function doIt(done) {
      detailsPromise.then(function onPromiseResolved(details) {
        debug('details %j', details);
        should(details).be.ok();
        done();
      }, function onPromiseRejected(err) {
        should.fail('promise was rejected');
        done(err);
      });
    });
  });
  describe.only('bug hunting', function indexDependencyGraph() {
    it('list all package repositories', function doIt(done) {
      debug('calling npmstats');
      var npmstats = new NpmStats();
      npmstats.getAllModules()
        .then(function onGetAllModulesResolved(modules) {
          async.eachSeries(modules, function getModuleDetails(m, nextModule) {
            debug('module %j', m);
            npmstats.get(m)
              .then(function getResolved(details) {
                if (details.repository && details.repository.url) {
                  console.log('%j', details.repository.url);
                }
                nextModule();
              }, function getRejected() {
                nextModule();
              });
          }, function onAllModuleDetailGotten() {
            done();
          });
        });
    });
  });
});
