var pageRank = require('pagerank-js');
var Q = require('q');
var NpmStats = require('./lib/npmstats');
var npmstats = new NpmStats();
var debug = require('debug')('noderank');

/**
 * class to caluclate the node rank for all modules in npm
 * @param {object} _npmstats optional npmstats object to build dependency graph with
 * @return {Promise} a promise is fulfilled with the dependency graph or rejected with a error
 */
function npmNodeRank(_npmstats) {
  var pageRankPromise = Q.defer();

  if (_npmstats) {
    npmstats = _npmstats;
  }
  debug('listing modules');

  debug('npmstats.getDependencies %j', npmstats.getDependencies);
  npmstats.buildDependencyGraph()
    .then(function onBuildDependencyGraphResolved(graph) {
      var normalizedGraph;
      var linkProb = 0.85;
      var tolerance = 0.0001;
      debug('buildDependencyGraph resolved %j', graph);
      normalizedGraph = npmstats.normalizeDependencyGraph(graph);
      debug('normalizedGraph %j', normalizedGraph);
      pageRank(normalizedGraph, linkProb, tolerance, function PageRankCallback(err, pagerankResults) {
        if (err) {
          return pageRankPromise.reject(new Error(err));
        }
        debug('returning pagerank');
        debug('pagerank %j', pagerankResults);
        // remove preceding # from pagerank keys
        for (var p in pagerankResults) {
          if (pagerankResults.hasOwnProperty(p)) {
            debug('fixing %s', p);
            pagerankResults[p.slice(1)] = pagerankResults[p];
            delete pagerankResults[p];
          }
        }
        debug('pagerankResults %j', pagerankResults);
        pageRankPromise.resolve(pagerankResults);
      });
    }, function onBuildDependencyGraphRejected(err) {
      debug('buildDependencyGraph rejected %j', err);
      pageRankPromise.reject(new Error(err));
    });

  return pageRankPromise.promise;
}
module.exports = {nodeRank: npmNodeRank, npmStats: npmstats};
