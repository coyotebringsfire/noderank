# noderank
Apply PageRank algorithm to npm repo

This module calculates the PageRank for all of the public modules in NPM. For these purposes each page is a module and the dependencies are links.

## Installing
```
$ npm install noderank
```
## Usage
The module exports a single function, which takes no arguments and returns a promise which is resolved on success or rejected on error.
```javascript
var npmNodeRank=require('noderank');

npmNodeRank()
  .then( function onResolve(noderank) {
    // use the noderank object
  }, function onReject(error) {
    // an error happened
  });
```
The object that is returned to the promise resolve handler is a hash whose keys are the names of the npm modules, and the value is the PageRank of the module. If there were three modules named '0', '1', and '2', each of which depends on the other two, the response might look like this: 
```javascript
{
  "0":0.3333333333333333,
  "1":0.3333333333333333,
  "2":0.3333333333333333
}
```
## Pre-generated Results
If you don't need up-to-date stats, you can use the pre-generated pagerank in results/pagerank