var noderank = require('./noderank');

noderank.nodeRank()
  .then(function onResolve(results) {
    console.log('RESOLVE');
    console.dir(results);
  }, function onReject(err) {
    console.log('REJECT');
    console.err(err.message);
  });
